/*
COPYRIGHT:
Frederik H�jgaard

*/

#include "stdafx.h"
#include<iostream>
#include<string>
#include<Windows.h>
#include<winsock.h>
#pragma comment(lib, "ws2_32.lib")
#include <io.h>
#include<ctime>
#include<chrono>
#include<fstream>
#include<corecrt.h>
#include<filesystem>
#include "location.h"
#include "C:\Users\pokem\source\repos\star\parser.h"

using std::string;

class backend {

	//Load the content of a http site.
	const char *h1 = "HTTP/1.1 200 OK\r\n";
	const char *h2 = "Content-Type: text/html\r\n\r\n";

	FILE *f;
	errno_t er;
	location l;

public: void loadStar(string path, int server, string getParameters) {
	
	build parser;

	for (string content : parser.run(path, getParameters)) {
		send(server, content.c_str(), strlen(content.c_str()), 0);
	}
}
public: void loadPage(string path, int server) {

	if (er = fopen_s(&f, path.c_str(), "r") == 0) {

		char content[255];
		send(server, h1, strlen(h1), 0);
		send(server, h2, strlen(h2), 0);

		while (fgets(content, 255, f)) {
			send(server, content, strlen(content), 0);
		}
		fclose(f);

	}
	else {

		send(server, "HTTP/1.1 404 Not Found\n", strlen("HTTP/1.1 404 Not Found\n"), 0);
		send(server, "Connection: Keep Alive\n", strlen("Connection: Keep  Alive\n"), 0);
		send(server, "Content-Type: html\n", strlen("Content-Type: html\n"), 0);

	}
}


};
