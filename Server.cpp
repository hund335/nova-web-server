/*
COPYRIGHT:
Frederik Højgaard

*/

#include "stdafx.h"
#include<iostream>
#include<string>
#include<Windows.h>
#include<winsock.h>
#pragma comment(lib, "ws2_32.lib")
#include <io.h>
#include<ctime>
#include<chrono>
#include<fstream>
#include<corecrt.h>
#include<filesystem>
#include "build.h"
#include "location.h"
#include<codecvt>

using std::cout;
using std::cin;
using std::endl;
using std::getline;
using std::string;
namespace fs = std::experimental::filesystem::v1;

int port = 0;

class run {

public: void setup() {

	WSADATA wsaData;
	int wsaret = WSAStartup(MAKEWORD(2, 2), &wsaData);

	int client = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (client < 0) {
		exit(1);
	}

	char opt;
	if (setsockopt(client, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))) {
		exit(1);
	}

	struct sockaddr_in sock;
	memset(&sock, 0, sizeof(struct sockaddr_in));
	sock.sin_family = AF_INET;
	sock.sin_port = htons(port);
	sock.sin_addr.S_un.S_addr = INADDR_ANY;

	if (bind(client, (struct sockaddr *)&sock, sizeof(sock)) == -1) {
		exit(1);
	}

	if (listen(client, 1) < 0) {
		exit(1);
	}

	int size = sizeof(sock);
	int server = accept(client, (struct sockaddr*)&sock, &size);

	if (server < 0) {
		exit(1);
	}

	char buffer[1024] = { 0 };
	int result;

	//runs it in a while loop until the content has been received.
	while (result = recv(server, buffer, 1024, 0) != 0) {

		//load the content when the content has successfully received the data
		if (result > 0) {
			string rea;

			for (int i = 0; i < buffer['\n']; i++) {
				rea += buffer[i];
				if (buffer[i] == '\n') break;
			}
			rea = rea.replace(rea.begin(), rea.begin() + 4, "");

			string get;
			string exe;
			string paremeters = "";
			if (get != "favicon.ico") {
				for (int i = 0; i < rea.size(); i++) {
					if (rea[i] == ' ') break;

					if (rea[i] != '?' && paremeters == "") {
						get += rea[i];
					}
					else {
						paremeters += rea[i];
					}
				}
			}
			backend build;
			location l;

			string newpath = l.getLocation() + get;
			string openindex;

			cout << "GET: " << get << paremeters << endl;


			//string exe = get.replace(get.begin(), get.end() - 3, "");

			//TODO TRYING TO REPLACE LESS THAN IT CAN

			if (get.size() >= 3) {
				exe = get;
				exe = exe.replace(exe.begin(), exe.end() - 4, "");
			}

			if (get == "/") {
				string tempexe;
				string index = l.getLocation();

				for (auto p : fs::directory_iterator(index)) {
					if (p.path().filename().replace_extension("") == "index" && p.path().has_extension() == true) {
						openindex = p.path().u8string();
						tempexe = p.path().extension().u8string();
						break;
					}


				}
				//open the defualt index file.
				if (tempexe != ".star") {
					build.loadPage(openindex, server);
				}
				else {
					build.loadStar(l.getLocation() + "/index.star", server, paremeters);
				}

			}
			//server side code execution.
			else if (exe == "star") {
				build.loadStar(l.getLocation() + get, server, paremeters);
			}
			//loading of normal html, css, json & js files.
			else {
				std::cout << "exe: " << exe;
				build.loadPage(newpath, server);
			}
			break;
		}
		else {

			//Will keeps to repeat the while loop until the data is received correctly
			if (WSAGetLastError() == WSAEMSGSIZE) {
				continue;
			}
			else {
				//Prints the error message out.
				cout << "Error " << WSAGetLastError();
			}
		}
	}
	closesocket(client);
	closesocket(server);


	//When a person visit the website
	printf("\n");
	const char *i = inet_ntoa(sock.sin_addr);
	printf("Visitor -> ");
	printf(i);
	printf("\n");

	setup();
}

};

int main()
{
	SetConsoleTitle(TEXT("Server: setup"));
	cout << "Location: " << endl;

	run r;
	location l;

	string loc;
	getline(cin, loc);
	l.setLocation(loc);

	string pt;
	cout << endl << "Port: " << endl;
	getline(cin, pt);
	cout << endl;

	port = std::stoi(pt);
	SetConsoleTitle(TEXT("Server: running"));
	r.setup();

	return 0;
}

